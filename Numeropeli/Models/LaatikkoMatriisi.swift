import Foundation


protocol Block {
    associatedtype Value

    var number: Value { get set }
}

struct IndeksoituLaatikko<T> where T: Block {
    typealias Indeksi = LaatikkoMatriisi<T>.Indeksi
    
    let indeksi: Self.Indeksi
    let olio: T
}

struct LaatikkoMatriisi<T> : CustomDebugStringConvertible where T: Block {
    typealias Indeksi = (Int, Int)
    var matriisi: [[T?]]
    
    init() {
        matriisi = [[T?]]()
        for _ in 0..<4 {
            var row = [T?]()
            for _ in 0..<4 {
                row.append(nil)
            }
            matriisi.append(row)
        }
    }
    
    var debugDescription: String {
        matriisi.map { row -> String in
            row.map {
                if $0 == nil {
                    return " "
                } else {
                    return String(describing: $0!.number)
                }
            }.joined(separator: "\t")
        }.joined(separator: "\n")
    }
    
    var muutaFlatMapiksi: [IndeksoituLaatikko<T>] {
        return self.matriisi.enumerated().flatMap { (y: Int, elementti: [T?]) in
            elementti.enumerated().compactMap { (x: Int, elementti: T?) in
                guard let olio = elementti else {
                    return nil
                }
                return IndeksoituLaatikko(indeksi: (x, y), olio: olio)
            }
        }
    }
    
    subscript(indeksi: Self.Indeksi) -> T? {
        guard indeksiOnValidi(indeksi) else {
            return nil
        }
        
        return matriisi[indeksi.1][indeksi.0]
    }

    mutating func liikuta(lahto: Self.Indeksi, loppu: Self.Indeksi) {
        guard indeksiOnValidi(lahto) && indeksiOnValidi(loppu) else {
            return
        }
        guard let source = self[lahto] else {
            return
        }
        
        matriisi[loppu.1][loppu.0] = source
        matriisi[lahto.1][lahto.0] = nil
    }
    
    mutating func liikuta(lahto: Self.Indeksi, loppu: Self.Indeksi, with uusiArvo: T.Value) {
        guard indeksiOnValidi(lahto) && indeksiOnValidi(loppu) else {
            return
        }
        
        guard var source = self[lahto] else {
            return
        }
        
        source.number = uusiArvo
        
        matriisi[loppu.1][loppu.0] = source
        matriisi[lahto.1][lahto.0] = nil
    }

    mutating func aseta(_ laatikko: T?, paate: Self.Indeksi) {
        matriisi[paate.1][paate.0] = laatikko
    }
    
     func indeksiOnValidi(_ indeksi: Self.Indeksi) -> Bool {
        guard indeksi.0 >= 0 && indeksi.0 < 4 else {
            return false
        }
        guard indeksi.1 >= 0 && indeksi.1 < 4 else {
            return false
        }
        return true
    }
}


