import Foundation
import SwiftUI
import Combine


final class Pelilogiikka : ObservableObject {
    
    // Tuodaan AppDelegate sisältämät metodit luokan saataville:
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    enum Suunta {
        case vasen
        case oikea
        case ylos
        case alas
    }
    
    typealias LaatikkoMatriisiType = LaatikkoMatriisi<YksittainenLaatikko>
    
    let olioVaihtuu = PassthroughSubject<Pelilogiikka, Never>()
    
    fileprivate var laatikkoMatriisi: LaatikkoMatriisiType!
    var LaatikkoMatriisi: LaatikkoMatriisiType {
        return laatikkoMatriisi
    }
    
    @Published
    fileprivate(set) var viimeisinLiikeSuunta: Suunta = .ylos
    
    fileprivate var _globaaliID = 0
    fileprivate var uusiGlobaaliID: Int {
        _globaaliID += 1
        return _globaaliID
    }
    
    init() {
        uusiPeli()
    }
    
    func uusiPeli() {
        laatikkoMatriisi = LaatikkoMatriisiType()
        resetoiViimeLiike()
        muodostaUudetLaatikot()
        olioVaihtuu.send(self)
    }
    
    func resetoiViimeLiike() {
        viimeisinLiikeSuunta = .ylos
    }
    
    func liikuta(_ suunta: Suunta) {
        
        defer {
            olioVaihtuu.send(self)
        }
        
        viimeisinLiikeSuunta = suunta
        
        var liikutettu = false
        
        let akseli = suunta == .vasen || suunta == .oikea
        for rivi in 0..<4 {
            var riviSnapShot = [YksittainenLaatikko?]()
            var yksiRivi = [YksittainenLaatikko]()
            for sarake in 0..<4 {
                if let laatikko = laatikkoMatriisi[akseli ? (sarake, rivi) : (rivi, sarake)] {
                    riviSnapShot.append(laatikko)
                    yksiRivi.append(laatikko)
                }
                riviSnapShot.append(nil)
            }
            
            // Päätellään onko tarvetta kääntää järjestys:
            let onKaannetty = suunta == .alas || suunta == .oikea
            
            yhdista(&yksiRivi, kaannetty: onKaannetty)
            
            var uusiRivi = [YksittainenLaatikko?]()
            yksiRivi.forEach { uusiRivi.append($0) }
            if yksiRivi.count < 4 {
                for _ in 0..<(4 - yksiRivi.count) {
                    if suunta == .vasen || suunta == .ylos {
                        uusiRivi.append(nil)
                    } else {
                        uusiRivi.insert(nil, at: 0)
                    }
                }
            }
            
            uusiRivi.enumerated().forEach {
                if riviSnapShot[$0]?.number != $1?.number {
                    liikutettu = true
                }
                laatikkoMatriisi.aseta($1, paate: akseli ? ($0, rivi) : (rivi, $0))
            }
        }
        
        if liikutettu {
            muodostaUudetLaatikot()
            self.appDelegate.toistaAani("laatikon_liike", type: "wav")
            
            // Lopuksi lasketaan pisteet kaikista ruuduista:
            var pisteet = 0
            for rivi in self.laatikkoMatriisi.matriisi {
                for sarake in rivi {
                    if let arvo = sarake?.number {
                        pisteet += arvo
                    }
                }
            }
            self.appDelegate.pisteArvo = String(pisteet)
        }
    }
    
    fileprivate func yhdista(_ laatikot: inout [YksittainenLaatikko], kaannetty: Bool) {
        if kaannetty {
            // Kääntää laatikoiden järjestyksen.
            laatikot = laatikot.reversed()
        }
        laatikot = laatikot
            .map { (false, $0) }
            .reduce([(Bool, YksittainenLaatikko)]()) { acc, olio in
                if acc.last?.0 == false && acc.last?.1.number == olio.1.number {
                    var accPrefix = Array(acc.dropLast())
                    var yhdistettyLaatikko = olio.1
                    yhdistettyLaatikko.number *= 2
                    accPrefix.append((true, yhdistettyLaatikko))
                    return accPrefix
                } else {
                    // Luodaan muuttujasta "deep-copy":
                    var accKopio = acc
                    accKopio.append((false, olio.1))
                    return accKopio
                }
            }
            .map { $0.1 }
        
        if kaannetty {
            laatikot = laatikot.reversed()
        }
    }
    
    @discardableResult fileprivate func muodostaUudetLaatikot() -> Bool {
        var tyhjatAlueet = [LaatikkoMatriisiType.Indeksi]()
        for riviIndeksi in 0..<4 {
            for sarakeIndeksi in 0..<4 {
                let indeksi = (sarakeIndeksi, riviIndeksi)
                if laatikkoMatriisi[indeksi] == nil {
                    tyhjatAlueet.append(indeksi)
                }
            }
        }
        
        guard tyhjatAlueet.count >= 2 else {
            return false
        }
        
        defer {
            olioVaihtuu.send(self)
        }
        
        var sijainninIndeksi = Int.random(in: 0..<tyhjatAlueet.count)
        laatikkoMatriisi.aseta(YksittainenLaatikko(id: uusiGlobaaliID, number: 2), paate: tyhjatAlueet[sijainninIndeksi])
        
        guard let viimeisinSijainti = tyhjatAlueet.last else {
            return false
        }
        tyhjatAlueet[sijainninIndeksi] = viimeisinSijainti
        sijainninIndeksi = Int.random(in: 0..<(tyhjatAlueet.count - 1))
        laatikkoMatriisi.aseta(YksittainenLaatikko(id: uusiGlobaaliID, number: 2), paate: tyhjatAlueet[sijainninIndeksi])
        
        return true
    }
}


