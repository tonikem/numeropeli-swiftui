import UIKit
import SwiftUI
import AVFoundation


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var audioSoitin: AVAudioPlayer?
    var peliLogiikka: Pelilogiikka!
    var peliIkkuna: UIWindow?
    var pisteArvo = "0"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        peliLogiikka = Pelilogiikka()
        peliIkkuna = UIWindow(frame: UIScreen.main.bounds)
        peliIkkuna!.rootViewController = UIHostingController(rootView:
            PeliNakyma().environmentObject(peliLogiikka)
        )
        peliIkkuna!.makeKeyAndVisible()
        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    func toistaAani(_ aani: String, type: String) {
        if let asset = NSDataAsset(name: aani){
           do {
                audioSoitin = try AVAudioPlayer(data:asset.data, fileTypeHint:type)
                audioSoitin?.play()
           }
           catch let error as NSError {
                print(error.localizedDescription)
           }
        }
    }
    
    @objc
    func uusiPeli(_ sender: AnyObject?) {
        withTransaction(Transaction(animation: .spring())) {
            peliLogiikka.uusiPeli()
            self.toistaAani("uusi_peli", type: "wav")
            print("Uusi peli aloitettu!")
        }
    }
    
    override func buildMenu(with builder: UIMenuBuilder) {
        builder.remove(menu: .edit)
        builder.remove(menu: .format)
        builder.remove(menu: .view)
        
        builder.replaceChildren(ofMenu: .file) { oldChildren in
            var newChildren = oldChildren  // Vaati tämän, jotta lapsioliot ovat mutable.
            let uusiPeliOlio = UIKeyCommand(input: "N", modifierFlags: .command, action: #selector(uusiPeli))
            uusiPeliOlio.title = "Uusi Peli"
            newChildren.insert(uusiPeliOlio, at: 0)
            return newChildren
        }
    }
}


