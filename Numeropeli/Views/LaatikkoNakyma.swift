import SwiftUI


struct LaatikkoNakyma : View {
    
     let varit: [(Color, Color)] = [
        (Color(red:0.90, green:0.70, blue:0.80, opacity:1.00), Color.white),  // 2
        (Color(red:0.70, green:0.86, blue:0.90, opacity:1.00), Color.white),  // 4
        (Color(red:0.55, green:0.88, blue:0.55, opacity:1.00), Color.white),  // 8
        (Color(red:0.93, green:0.67, blue:0.46, opacity:1.00), Color.white),  // 16
        (Color(red:0.35, green:0.35, blue:0.80, opacity:1.00), Color.white),  // 32
        (Color(red:0.74, green:0.35, blue:0.23, opacity:1.00), Color.white),  // 64
        (Color(red:0.91, green:0.78, blue:0.43, opacity:1.00), Color.white),  // 128
        (Color(red:0.05, green:0.58, blue:0.87, opacity:1.00), Color.white),  // 256
        (Color(red:0.90, green:0.77, blue:0.31, opacity:1.00), Color.white),  // 512
        (Color(red:0.91, green:0.95, blue:0.84, opacity:1.00), Color.white),  // 1024
        (Color(red:0.05, green:0.05, blue:0.05, opacity:1.00), Color.white)   // 2048
    ]
    
    let numero: Int?
    let id: String?
    
    init(laatikko: YksittainenLaatikko) {
        self.numero = laatikko.number
        self.id = "\(laatikko.id):\(laatikko.number)"
    }
    
    init() {
        self.numero = nil
        self.id = ""
    }
    
    static func tyhjenna() -> Self {
        return self.init()
    }
    
     var numeroTeksti: String {
        guard let number = numero else {
            return ""
        }
        return String(number)
    }
    
     var fonttiKoko: CGFloat {
        let tekstinPituus = numeroTeksti.count
        if tekstinPituus < 3 {
            return 32
        } else if tekstinPituus < 4 {
            return 18
        } else {
            return 12
        }
    }
    
     var variPari: (Color, Color) {
        guard let numero = numero else {
            return (Color(red:0.91, green:0.94, blue:0.91, opacity:1.00), Color.black)
        }
        let indeksi = Int(log2(Double(numero))) - 1
        if indeksi < 0 || indeksi >= varit.count {
            fatalError("Väriä ei löytynyt indeksille: \(indeksi)")
        }
        return varit[indeksi]
    }
    
    var body: some View {
        ZStack {
            Rectangle()
                .fill(variPari.0)
                .zIndex(1.00)
            Text(numeroTeksti)
                .font(Font.system(size: fonttiKoko).bold())
                .foregroundColor(variPari.1)
                .id(id!)
                .zIndex(1000.00)
                .transition(AnyTransition.opacity.combined(with: .scale))
        }
        .clipped()
        .cornerRadius(6)
    }
}


