import SwiftUI
import AVFoundation


extension Edge {
    static func from(_ from: Pelilogiikka.Suunta) -> Self {
        switch from {
        case .alas:
            return .top
        case .ylos:
            return .bottom
        case .vasen:
            return .trailing
        case .oikea:
            return .leading
        }
    }
    
}

struct PeliNakyma : View {
    
    @EnvironmentObject
    var Pelilogiikka: Pelilogiikka
    
    // Taustamusiikin soittamiseen:
    var taustaMusiikki: AVAudioPlayer?
    
    // Tuodaan AppDelegate sisältämät metodit luokan saataville:
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    init() {
        if let mp3 = NSDataAsset(name: "taustamusiikki") {
           do {
                taustaMusiikki = try AVAudioPlayer(data:mp3.data, fileTypeHint:"mp3")
                taustaMusiikki?.numberOfLoops = -1
                taustaMusiikki?.play()
           }
           catch let error as NSError {
                print(error.localizedDescription)
           }
        }
    }
    
    var liike: some Gesture {
        let raja_arvo: CGFloat = 44
        let _liike = DragGesture().onChanged { v in
                guard abs(v.translation.width) > raja_arvo ||
                    abs(v.translation.height) > raja_arvo else {
                    return
                }
                withTransaction(Transaction(animation: .spring())) {
                    if v.translation.width > raja_arvo {
                        self.Pelilogiikka.liikuta(.oikea)
                    }
                    else if v.translation.width < -raja_arvo {
                        self.Pelilogiikka.liikuta(.vasen)
                    }
                    else if v.translation.height > raja_arvo {
                        self.Pelilogiikka.liikuta(.alas)
                    }
                    else if v.translation.height < -raja_arvo {
                        self.Pelilogiikka.liikuta(.ylos)
                    }
                }
            }
        return _liike
    }
    
    var content: some View {
        GeometryReader { proxy in
            ZStack(alignment: .top) {
                Text("Numeropeli")
                    .font(Font.system(size: 48).weight(.black))
                    .foregroundColor(Color(red:0.2, green:0.2, blue:0.2, opacity:1.00))
                    .offset(.init(width: 0, height: 32))
                
                ZStack(alignment: .center) {
                    RuudukkoNakyma(
                        matriisi: self.Pelilogiikka.LaatikkoMatriisi,
                        laatikonReuna: .from(self.Pelilogiikka.viimeisinLiikeSuunta)
                    )
                    
                    Text(self.appDelegate.pisteArvo)
                        .id("Pisteiden Lasku Kentta")
                        .font(Font.system(size: 36).weight(.black))
                        .foregroundColor(Color(red:0.3, green:0.3, blue:0.3, opacity:1.00))
                        .offset(.init(width: 0, height: -235))
                    
                    Button(action: {
                        self.appDelegate.uusiPeli(nil)
                    }) {
                        Text("Uusi Peli")
                            .fontWeight(.bold)
                            .font(.title)
                            .padding()
                            .foregroundColor(.white)
                            .padding(10)
                            .background(
                                LinearGradient(
                                    gradient: Gradient(colors: [
                                        Color(red:0.90, green:0.75, blue:0.85, opacity:1),
                                        Color(red:0.20, green:0.20, blue:0.20, opacity:1)
                                    ]),
                                    startPoint: .top, endPoint: .bottom
                                )
                            )
                            .cornerRadius(40)
                    }
                    .offset(y: 270)
                }
                .frame(width: proxy.size.width, height: proxy.size.height, alignment: .center)
            }
            .frame(width: proxy.size.width, height: proxy.size.height, alignment: .center)
            .background(
                Rectangle()
                    .fill(Color(red:1, green:0.90, blue:0.90, opacity:1.00))
                    .edgesIgnoringSafeArea(.all)
            )
        }
    }
    
    var body: some View {
        return content.gesture(liike, including: .all)
    }
}


