import SwiftUI


struct AnkkuroituSkaalaus : ViewModifier {
    let skaalausLuku: CGFloat
    let ankkuri: UnitPoint
    
    func body(content: Self.Content) -> some View {
        content.scaleEffect(skaalausLuku, anchor: ankkuri)
    }
    
}

struct HaivytysEfekti : ViewModifier {
    let haivytysArvo: CGFloat
    
    func body(content: Self.Content) -> some View {
        content.blur(radius: haivytysArvo)
    }
    
}

struct YhdistettyNakymanMuuttaja<M1, M2> : ViewModifier where M1: ViewModifier, M2: ViewModifier {
    let m1: M1
    let m2: M2
    
    init(ensimmainen: M1, toinen: M2) {
        m1 = ensimmainen
        m2 = toinen
    }
    
    func body(content: Self.Content) -> some View {
        content.modifier(m1).modifier(m2)
    }
    
}

extension AnyTransition {
    static func laatikonLuonti(raja: Edge, sijainti: CGPoint, `rect`: CGRect) -> AnyTransition {
        let ankkuri = UnitPoint(x: sijainti.x / `rect`.width, y: sijainti.y / `rect`.height)
        return .asymmetric(
            insertion: AnyTransition.opacity.combined(with: .move(edge: raja)),
            removal: AnyTransition.opacity.combined(with: .modifier(
                active: YhdistettyNakymanMuuttaja(
                    ensimmainen: AnkkuroituSkaalaus(skaalausLuku: 0.8, ankkuri: ankkuri),
                    toinen: HaivytysEfekti(haivytysArvo: 20)
                ),
                identity: YhdistettyNakymanMuuttaja(
                    ensimmainen: AnkkuroituSkaalaus(skaalausLuku: 1.0, ankkuri: ankkuri),
                    toinen: HaivytysEfekti(haivytysArvo: 0)
                )
            ))
        )
    }
}

struct RuudukkoNakyma : View {
    let matriisi: LaatikkoMatriisi<YksittainenLaatikko>
    let laatikonReuna: Edge
    
    func luoLaatikko(_ laatikko: YksittainenLaatikko?,
                     at indeksi: IndeksoituLaatikko<YksittainenLaatikko>.Indeksi) -> some View {
        
        let laatikkoNakyma: LaatikkoNakyma
        
        if let uuusiLaatikko = laatikko {
            laatikkoNakyma = LaatikkoNakyma(laatikko: uuusiLaatikko)
        } else {
            laatikkoNakyma = LaatikkoNakyma.tyhjenna()
        }
        
        let laatikonKoko: CGFloat = 65
        let tila: CGFloat = 12
        
        let position = CGPoint(
            x: CGFloat(indeksi.0) * (laatikonKoko + tila) + laatikonKoko / 2 + tila,
            y: CGFloat(indeksi.1) * (laatikonKoko + tila) + laatikonKoko / 2 + tila
        )
        
        return laatikkoNakyma
            .frame(width: laatikonKoko, height: laatikonKoko, alignment: .center)
            .position(x: position.x, y: position.y)
            .transition(.laatikonLuonti(
                raja: self.laatikonReuna,
                sijainti: CGPoint(x: position.x, y: position.y),
                rect: CGRect(x: 0, y: 0, width: 320, height: 320)
            ))
    }
    
    func zIndex(_ laatikko: YksittainenLaatikko?) -> Double {
        if laatikko == nil {
            return 1.00
        }
        return 1000.00
    }
    
    var body: some View {
        ZStack {
            ForEach(0..<4) { x in
                ForEach(0..<4) { y in
                    self.luoLaatikko(nil, at: (x, y))
                }
            }
            .zIndex(1.00)
            
            ForEach(self.matriisi.muutaFlatMapiksi, id: \.olio.id) {
                self.luoLaatikko($0.olio, at: $0.indeksi)
            }
            .zIndex(1000.00)
            .animation(.interactiveSpring(response: 0.4, dampingFraction: 0.8))
            
        }
        .frame(width: 320, height: 320, alignment: .center)
        .background(
            Rectangle()
                .fill(Color(red:0.2, green:0.2, blue:0.2, opacity:1.00))
        )
        .clipped()
        .cornerRadius(6)
        .drawingGroup(opaque: false, colorMode: .linear)
    }
}


